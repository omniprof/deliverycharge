package com.cejv416.deliverycharge;

import com.cejv416.deliverycharge.data.DeliveryChargeBean;
import java.text.NumberFormat;
import java.util.Scanner;

/**
 * DeliveryApp structured code example
 *
 * @author Ken
 */
public class DeliveryAppWithBean {

    // Declare and initialize constant values
    final private double ZONE1 = 2.0;
    final private double ZONE2 = 3.0;
    final private double ZONE3 = 5.0;
    final private double ZONE4 = 6.0;
    final private double GST = 0.05;
    final private double PST = 0.09975;

    /**
     * Input Ask the user for a zone, rejecting anything other than 1,2,3 or 4
     *
     * @param delivery
     */
    public void requestZone(DeliveryChargeBean delivery) {
        // Declaration
        int zone;

        Scanner sc = new Scanner(System.in);
        do {
            // Primary Input
            System.out.println("Please enter a Zone from 1 - 4: ");
            // Check if there is an integer waiting in the buffer
            if (sc.hasNextInt()) {
                // There is so get it
                zone = sc.nextInt();
            } else {
                // Invalid input so set zone to -1
                zone = -1;
                System.out.println("I'm sorry, your input was invalid.");
            }
            // Clean out invalid characters
            sc.nextLine();
        } while (zone < 1 || zone > 4);
        
        delivery.setZone(zone);
    }

    /**
     * Process part 1 Determine the base charge based on the zone
     *
     * @param delivery
     */
    public void determineBaseCharge(DeliveryChargeBean delivery) {
        // Declaration        
        double baseCharge;

        // Original switch syntax
//        switch (delivery.getZone()) {
//            case 1:
//                baseCharge = ZONE1;
//                break;
//            case 2:
//                baseCharge = ZONE2;
//                break;
//            case 3:
//                baseCharge = ZONE3;
//                break;
//            case 4:
//                baseCharge = ZONE4;
//                break;
//            default:
//                // Somehow we have an invalid zone
//                baseCharge = 0.0;
//        }
        
        // Switch Expression
        baseCharge = switch (delivery.getZone()) {
            case 1 -> ZONE1;
            case 2 -> ZONE2;
            case 3 -> ZONE3;
            case 4 -> ZONE4;
            default -> 0.0;
        };

        delivery.setBaseCharge(baseCharge);
    }

    /**
     * Process Part 2 Calculate the the GST, PST, and total charge
     *
     * @param delivery
     */
    public void calculateCharge(DeliveryChargeBean delivery) {
        // Declarations
        double gstCharge = 0.0;
        double pstCharge = 0.0;
        double totalCharge = 0.0;

        // Will only be less than 0 if zone number is invalid
        if (delivery.getBaseCharge() > 0) {
            gstCharge = delivery.getBaseCharge() * GST;
            pstCharge = delivery.getBaseCharge() * PST;
            totalCharge = delivery.getBaseCharge() + gstCharge + pstCharge;
        }
        delivery.setGstCharge(gstCharge);
        delivery.setPstCharge(pstCharge);
        delivery.setTotalCharge(totalCharge);
    }

    /**
     * Output Display the result formatted to currency
     *
     * @param delivery
     */
    public void displayResults(DeliveryChargeBean delivery) {
        NumberFormat money = NumberFormat.getCurrencyInstance();
        System.out.println("Base Charge: " + money.format(delivery.getBaseCharge()));
        System.out.println("        GST: " + money.format(delivery.getGstCharge()));
        System.out.println("        PST: " + money.format(delivery.getPstCharge()));
        System.out.println("      Total: " + money.format(delivery.getTotalCharge()));
    }

    /**
     * Carry out the steps in the correct sequence
     */
    public void perform() {
        DeliveryChargeBean deliveryCharges = new DeliveryChargeBean();
        requestZone(deliveryCharges);
        determineBaseCharge(deliveryCharges);
        calculateCharge(deliveryCharges);
        displayResults(deliveryCharges);
    }

    /**
     * This is where the program begins
     *
     * @param args
     */
    public static void main(String[] args) {
        new DeliveryAppWithBean().perform();
    }
}
