package com.cejv416.deliverycharge;

import java.text.NumberFormat;
import java.util.Scanner;

/**
 * DeliveryApp structured code example
 *
 * @author Ken
 */
public class DeliveryApp {

    // Declare and initialize constant values
    final private double ZONE1 = 2.0;
    final private double ZONE2 = 3.0;
    final private double ZONE3 = 5.0;
    final private double ZONE4 = 6.0;
    final private double GST = 0.05;
    final private double PST = 0.09975;

    /**
     * Input Ask the user for a zone, rejecting anything other than 1,2,3 or 4
     *
     * @return The user inputted zone
     */
    public int requestZone() {
        // Declaration
        int zone;

        // Initialize SCanner for keyboard input
        Scanner sc = new Scanner(System.in);
        do {
            System.out.println("Please enter a Zone from 1 - 4: ");
            // Check if there is an integer waiting in the buffer
            if (sc.hasNextInt()) {
                // There is so get it
                zone = sc.nextInt();
            } else {
                // Invalid input so set zone to -1
                zone = -1;
                System.out.println("I'm sorry, your input was invalid.");
            }
            // Clean out invalid characters
            sc.nextLine();
        } while (zone < 1 || zone > 4);
        return zone;
    }

    /**
     * Process part 1 Determine the base charge based on the zone
     *
     * @param zone
     * @return the base charge based on the zone
     */
    public double determineBaseCharge(int zone) {
        // Declaration        
        double baseCharge;

        // Original switch syntax
//        switch (zone) {
//            case 1:
//                baseCharge = ZONE1;
//                break;
//            case 2:
//                baseCharge = ZONE2;
//                break;
//            case 3:
//                baseCharge = ZONE3;
//                break;
//            case 4:
//                baseCharge = ZONE4;
//                break;
//            default:
//                // Somehow we have an invalid zone
//                baseCharge = 0.0;
//        }
        // Switch Expression
        baseCharge = switch (zone) {
            case 1 ->
                ZONE1;
            case 2 ->
                ZONE2;
            case 3 ->
                ZONE3;
            case 4 ->
                ZONE4;
            default ->
                0.0;
        };

        return baseCharge;
    }

    /**
     * Process Part 2 Calculate the the GST, PST, and total charge
     *
     * @param baseCharge
     * @return the total charge including taxes
     */
    public double calculateCharge(double baseCharge) {
        // Declarations
        double gstCharge;
        double pstCharge;
        double totalCharge = 0.0;

        // Will only be less than 0 if zone number is invalid
        if (baseCharge > 0) {
            gstCharge = baseCharge * GST;
            pstCharge = baseCharge * PST;
            totalCharge = baseCharge + gstCharge + pstCharge;
        }
        return totalCharge;
    }

    /**
     * Output Display the result formatted to currency
     *
     * @param baseCharge
     * @param totalCharge
     */
    public void displayResults(double baseCharge, double totalCharge) {
        NumberFormat money = NumberFormat.getCurrencyInstance();
        System.out.println("Base Charge: " + money.format(baseCharge));
        System.out.println("      Total: " + money.format(totalCharge));
    }

    /**
     * Carry out the steps in the correct sequence
     */
    public void perform() {
        int zone = requestZone();
        double baseCharge = determineBaseCharge(zone);
        double totalCharge = calculateCharge(baseCharge);
        displayResults(baseCharge, totalCharge);
    }

    /**
     * This is where the program begins
     *
     * @param args
     */
    public static void main(String[] args) {
        new DeliveryApp().perform();
    }
}
