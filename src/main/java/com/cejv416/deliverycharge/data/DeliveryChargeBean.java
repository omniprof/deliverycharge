package com.cejv416.deliverycharge.data;

/**
 * Bean/data class for managing multiple values
 *
 * @author Ken Fogel
 */
public class DeliveryChargeBean {

    // Declaration
    private int zone;
    private double baseCharge;
    private double gstCharge;
    private double pstCharge;
    private double totalCharge;

    // Getters and setters
    public int getZone() {
        return zone;
    }

    public void setZone(int zone) {
        this.zone = zone;
    }

    public double getBaseCharge() {
        return baseCharge;
    }

    public void setBaseCharge(double baseCharge) {
        this.baseCharge = baseCharge;
    }

    public double getGstCharge() {
        return gstCharge;
    }

    public void setGstCharge(double gstCharge) {
        this.gstCharge = gstCharge;
    }

    public double getPstCharge() {
        return pstCharge;
    }

    public void setPstCharge(double pstCharge) {
        this.pstCharge = pstCharge;
    }

    public double getTotalCharge() {
        return totalCharge;
    }

    public void setTotalCharge(double totalCharge) {
        this.totalCharge = totalCharge;
    }
}
